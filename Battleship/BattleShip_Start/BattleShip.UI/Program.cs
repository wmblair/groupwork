﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.UI.Workflow;

namespace BattleShip.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            //instantiate a new instance of a workflow class (folder) that will itself have two instantiated board objects
            //and then the rest of the functionality in a do...while structure will be in the workflow class
            //while the game is being played

            StartMenu start = new StartMenu();
              Console.ForegroundColor = ConsoleColor.Red;
              Console.WriteLine("  ______ " +  " ______ " + " _______"  + " _______" + " _     " + " ______" + " _____ " + " _    _ " + " _______" + " _______" +
                              "\n / ___  |" +  "/  __  |" + "/_   __/"  + "/_   __/" + "/ |    " + "/ ____/" + "/ ____/" + "/ |  / |" + "/_   __/" + "/  __  |" +
                              "\n | |  | |" +  "| |  | |" + "  | |   "  + "  | |   " + "| |    " + "| |    " + "| |    " + "| |  | |" + "  | |   " + "| |  | |" +
                              "\n | |__/ |" +  "| |__| |" + "  | |   "  + "  | |   " + "| |    " + "| |____" + "| |___ " + "| |__| |" + "  | |   " + "| |__/ |" +
                              "\n | ___  /" +  "|  __  |" + "  | |   "  + "  | |   " + "| |    " + "|  ___/" + "|___  |" + "|  __  |" + "  | |   " + "|  ____/" +
                              "\n | |  | |" +  "| |  | |" + "  | |   "  + "  | |   " + "| |    " + "| |    " + "    | |" + "| |  | |" + "  | |   " + "| |     " +
                              "\n | |__/ |" +  "| |  | |" + "  | |   "  + "  | |   " + "| |____" + "| |____" + " ___/ |" + "| |  | |" + " _| |___" + "| |     " +
                              "\n |______/" +  "|_/  |_/" + "  |_/   "  + "  |_/   " + "|_____/" + "|_____/" + "|_____/" + "|_/  |_/" + "|______/" + "|_/     " +
                              "\n");
            Console.ForegroundColor = ConsoleColor.White;

            start.StoreNames();
            BattleshipWorkflow workflow = new BattleshipWorkflow();
            do
            {
                workflow.Execute(start);
                Console.Write("\nDo you want to Play Again? Press Y for Yes: ");
                string playAgain = Console.ReadKey().Key.ToString();
                Console.Clear();
                if (playAgain.ToUpper() != "Y")
                    break;
            } while (true);
        }
    }
}
