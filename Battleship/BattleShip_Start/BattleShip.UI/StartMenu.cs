﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShip.UI
{
    public class StartMenu
    {
        public string[] playerNames;

        public StartMenu()
        {
             playerNames = new string[2];
        }

        public void StoreNames()
        {
            Console.WriteLine("*****Welcome to Battleship!*****");

            Console.Write("\nPlease enter player1's name: ");
            playerNames[0] = Console.ReadLine();
            Console.Write("\nPlease enter player2's name: ");
            playerNames[1] = Console.ReadLine();

            Console.Write("\n*******Time to Play!*******");
            Console.ReadLine();
            Console.Clear();
        }
    }
}
