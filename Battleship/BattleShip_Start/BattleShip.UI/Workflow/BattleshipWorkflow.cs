﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;

namespace BattleShip.UI.Workflow
{
    public class BattleshipWorkflow
    {
        public Board[] pBoard { get; set; }

        public BattleshipWorkflow()
        {
            pBoard = new Board[] {new Board(), new Board()};
        }

        public BattleshipWorkflow(Board board) //Only use in nUnit for testing the other methods
        {
            pBoard = new Board[] { board, board };
        }

        public void Execute(StartMenu start)
        {
            for (int whoPlay = 0; whoPlay < 2; whoPlay++)
                PlayerPlaceShips(whoPlay, start); //Execute this 2 times for 2 players

            PlayGame(start);
        }

        public void PlayerPlaceShips(int whoPlay, StartMenu start)
        {
            Console.WriteLine(start.playerNames[whoPlay] + ": ");
            for (int shipCount = 0; shipCount < 5; shipCount++)
                PlayerPlace1Ship(whoPlay, shipCount); //execute this 5 times for each ship
            Console.Clear();
        }

        public void PlayerPlace1Ship(int whoPlay, int shipCount)
        {
            string nameShip = Enum.GetName(typeof(ShipType), shipCount);
            do
            {
                Coordinate coord = AskForCoordinate(nameShip);

                string direction = AskForDirections(nameShip, shipCount, whoPlay, coord);
                ShipPlacement shipPlac = Create1ShipIfGoodStatus(whoPlay, shipCount, direction, coord);

                if (IsValidDirection(direction, (int)shipPlac))
                {
                    Console.WriteLine(shipPlac.ToString());
                    break;
                }
            } while (true);
        }

        public Coordinate AskForCoordinate(string nameShip)
        {
            string strCoord;

            do
            {
                Console.Write("\nPlease enter a coordinate for " + nameShip + ": ");
                strCoord = Console.ReadLine();

                if (IsValidCoordinate(strCoord))
                    break;     
            } while (true);

            return ConvertStringToCoordinate(strCoord);
        }


        public string AskForDirections(string nameShip, int shipCount, int whoPlay, Coordinate coord)
        {
            string dir;
            do
            {
                Console.Write("\nEnter a direction for " + nameShip + " (0=Up, 1=down, 2=left, 3=right): ");
                dir = Console.ReadLine();

                if (IsValidDirection(dir))
                    break;
            } while (true);
            return dir;
        }

        public ShipPlacement Create1ShipIfGoodStatus(int whoPlay, int shipCount, string direction, Coordinate coord)
        {
            PlaceShipRequest pRequest = new PlaceShipRequest
            {
                Direction = (ShipDirection)int.Parse(direction),
                ShipType = (ShipType)shipCount,
                Coordinate = coord
            };
            return pBoard[whoPlay].PlaceShip(pRequest);
        }

        public void PlayGame(StartMenu start)
        {
            int whoPlay = 0;
            do
            {
                Console.Write(start.playerNames[whoPlay] + "'s turn");
                SetupGraph(pBoard[whoPlay].ShotHistory);
                FireShotResponse shotAnswer = PromptPlayer(whoPlay);

                Console.Write("Result:");
                SetupGraph(pBoard[whoPlay].ShotHistory);
                DisplayShotAnswer(shotAnswer, whoPlay);
                whoPlay = (whoPlay + 1) % 2;
                
                if (((int)shotAnswer.ShotStatus) == 5)
                    break;
                Console.ReadLine();
                Console.Clear();
            } while (true);
        }

        public FireShotResponse PromptPlayer(int whichPlayer)
        {
            FireShotResponse fireShot;
            do
            {
                Console.Write("\nWhat coordinates do you want to fire at? ");
                string shot = Console.ReadLine();

                if (!IsValidCoordinate(shot))
                    continue;

                fireShot = pBoard[whichPlayer].FireShot(ConvertStringToCoordinate(shot));
                if (IsValidCoordinate(shot, (int)fireShot.ShotStatus))
                    break;
            } while (true);
            Console.Clear();
            return fireShot;
        }

        public void DisplayShotAnswer(FireShotResponse shotHistory, int whoPlay)
        {
            string answer = shotHistory.ShotStatus.ToString();

            switch ((int)shotHistory.ShotStatus)
            {
                case 2:
                    Console.Write("\n" + answer + " -Your projectile splashes into the ocean, you missed! - next player's turn");                   
                    break;
                case 3:
                    Console.Write("\n" + answer + " -You hit something! - Next player's turn");
                    break;
                case 4:
                    Console.Write("\n" + answer + " -You sank your opponent's " + shotHistory.ShipImpacted.ToString() + " - next player's turn");
                    break;
                case 5:
                    Console.Write("\n" + answer + " -You have sunk all your opponent's ships, you win! - end game");
                    break;
            }
        }

        public void SetupGraph(Dictionary<Coordinate, ShotHistory> dictionaryHistory) //keytype is coordinate, value type is enum (collection of hit, miss and unknown)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            for (int i = 0; i < 21; i++)
            {
                for (int j = 0; j < 11; j++)
                {
                    Coordinate newLocation = new Coordinate(j,(i/2));
                    if (j == 0)
                    {
                        if (i == 0)
                            Console.Write("\n\n    ");
                        else if (i == 20)
                            Console.Write(" " + (i/2) + " "); //10 takes more space
                        else if (i%2 == 0)
                            Console.Write("  " + (i/2) + " "); // 
                        else
                            Console.Write(" ----");
                    }
                    else if (i == 0)
                        Console.Write("| " + Convert.ToChar(j + 64) + " "); //Show A ... J
                    else if (i%2 == 1)
                        Console.Write("----");
                    else if (dictionaryHistory.ContainsKey(newLocation))
                    {
                        Console.Write("| ");
                        if (dictionaryHistory[newLocation].ToString() == "Hit") //if the new key value pair contains "Hit"
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write("H ");
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.Write("M ");
                        }
                        Console.ForegroundColor = ConsoleColor.Cyan;
                    }
                    else
                        Console.Write("|   ");
                }
                Console.WriteLine();
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        public Coordinate ConvertStringToCoordinate(string s)
        {
            int intNumber = int.Parse(s.Substring(1));
            Coordinate c = new Coordinate(TranslateCoordinates(s[0]), intNumber);
            return c;
        }

        public int TranslateCoordinates(char coordinate)
        {
            int index = char.ToUpper(coordinate) - 64;
            return index;
        }

        public bool IsValidCoordinate(string s, int status = 2)
        {
            int intNumber;
            if (s == "")
                Console.WriteLine("\nPlease enter something! Try again");
            else if (status == 0)
                Console.WriteLine("\nInvalid!  Try again");
            else if (status == 1)
                Console.WriteLine("\nDuplicate!  Try again");
            else if (!(int.TryParse(s.Substring(1), out intNumber)))
                Console.WriteLine("\nInvalid format! Try again");
            else
            {
                Coordinate coor = ConvertStringToCoordinate(s);
                if (!(coor.XCoordinate >= 1 && coor.XCoordinate <= 10 &&
                coor.YCoordinate >= 1 && coor.YCoordinate <= 10))
                    Console.WriteLine("\nCoordinate does not exist! Try again");
                else
                    return true;
            }   
            return false;
        }

        public bool IsValidDirection(string direction, int status = 2)
        {
            int intDir;
            if (direction == "")
                Console.WriteLine("\nPlease enter something! Try Again");
            else if (status == 0)
                Console.WriteLine("\nNot enough space! Please enter a direction that doesn't go off the board. Try Again");
            else if (status == 1)
                Console.WriteLine("\nYour ships are overlapping! Please enter a direction that doesn't overlap your ships Try Again");  //if you put in a coordinate that is on another ship, you can't put in any direction! :(
            else if(!(int.TryParse(direction, out intDir)))
                Console.WriteLine("\nYour input is not a number! Try Again");
            else if(intDir < 0 || intDir > 3)
                Console.WriteLine("\nInvalid direction! Try Again");
           else
                return true;
            return false;
        }
    }
}
