﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using BattleShip.BLL.GameLogic;
using BattleShip.BLL.Requests;
using BattleShip.BLL.Responses;
using BattleShip.BLL.Ships;
using BattleShip.UI.Workflow;

namespace Battleship.Tests
{
    [TestFixture]
    class WorkFlowTests
    {
        private BattleshipWorkflow _battleship;
        private Board board;

        [SetUp]
        public void BeforeEachTest()
        {
            var board = SetupBoard();
            _battleship = new BattleshipWorkflow(board);
        }

        #region "Board Setup"
        /// <summary>
        /// Let's set up a board as follows:
        /// Cruiser: (3,1) (3,2) (3,3)
        /// Sub: (1,5) (2,5) (3,5)
        /// Battleship: (10,6) (10,7) (10,8) (10, 9)
        /// Carrier: (4,4) (5,4) (6,4) (7,4) (8,4)
        /// 
        ///    1 2 3 4 5 6 7 8 9 10
        ///  1     R
        ///  2     R
        ///  3     R
        ///  4       C C C C C
        ///  5 S S S
        ///  6                   B
        ///  7                   B
        ///  8                   B
        ///  9                   B
        /// 10
        /// </summary>
        /// <returns>A board that is ready to play</returns>
        private Board SetupBoard()
        {
            Board board = new Board();

            //PlaceDestroyer(board);
            PlaceCruiser(board);
            PlaceSubmarine(board);
            PlaceBattleship(board);
            PlaceCarrier(board);

            return board;
        }

        private void PlaceCarrier(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(4, 4),
                Direction = ShipDirection.Right,
                ShipType = ShipType.Carrier
            };

            board.PlaceShip(request);
        }

        private void PlaceBattleship(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(10, 6),
                Direction = ShipDirection.Down,
                ShipType = ShipType.Battleship
            };

            board.PlaceShip(request);
        }

        private void PlaceSubmarine(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(3, 5),
                Direction = ShipDirection.Left,
                ShipType = ShipType.Submarine
            };

            board.PlaceShip(request);
        }

        private void PlaceCruiser(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(3, 3),
                Direction = ShipDirection.Up,
                ShipType = ShipType.Cruiser
            };

            board.PlaceShip(request);
        }

        private void PlaceDestroyer(Board board)
        {
            var request = new PlaceShipRequest()
            {
                Coordinate = new Coordinate(1, 8),
                Direction = ShipDirection.Right,
                ShipType = ShipType.Destroyer
            };

            board.PlaceShip(request);
        }
        #endregion

        [TestCase(0, 0, "1", 1, 1, ShipPlacement.Ok)]
        [TestCase(0, 0, "1", 3, 3, ShipPlacement.Overlap)]
        [TestCase(1, 0, "2", 1, 2, ShipPlacement.NotEnoughSpace)]
        public void CheckCreate1ShipIfGoodStatus(int whoPlay, int shipCount, string direction, int XCoord, int YCoord, ShipPlacement expected)
        {
            ShipPlacement result = _battleship.Create1ShipIfGoodStatus(whoPlay, shipCount, direction, new Coordinate(XCoord, YCoord));
            Assert.AreEqual(expected, result);
        }

        [TestCase("a1", 1, 1)]
        [TestCase("G2", 7, 2)]
        [TestCase("B10", 2, 10)]
        public void CheckConvertStringToCoordinate(string s, int expectedX, int expectedY)
        {
            Coordinate result = _battleship.ConvertStringToCoordinate(s);
            Assert.AreEqual(new Coordinate(expectedX, expectedY), result);
        }

        [TestCase('a', 1)]
        [TestCase('D', 4)]
        [TestCase('E', 5)]
        public void CheckTranslateCoordinates(char coordinate, int expected)
        {
            int result = _battleship.TranslateCoordinates(coordinate);
            Assert.AreEqual(expected, result);
        }

        [TestCase("a1", 0, false)]
        [TestCase("a1", 1, false)]
        [TestCase("", 3, false)]
        [TestCase("aa", 3, false)]
        [TestCase("1a", 3, false)]
        [TestCase("a11", 3, false)]
        [TestCase("k1", 3, false)]
        [TestCase("a1", 3, true)]
        public void CheckIsValidCoordinate(string s, int status, bool expected)
        {
            bool result = _battleship.IsValidCoordinate(s, status);
            Assert.AreEqual(expected, result);
        }

        [TestCase("1", 0, false)]
        [TestCase("1", 1, false)]
        [TestCase("", 3, false)]
        [TestCase("aa", 3, false)]
        [TestCase("4", 3, false)]
        [TestCase("1", 3, true)]
        public void CheckIsValidDirection(string s, int status, bool expected)
        {
            bool result = _battleship.IsValidDirection(s, status);
            Assert.AreEqual(expected, result);
        }

    }


}
