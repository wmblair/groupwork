﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.BLL
{
    public class AccountManager
    {
        public Response<List<Account>> GetAllAccounts()
        {
            var repo = new AccountRepository();
            var result = new Response<List<Account>>();

            try
            {
                var accounts = repo.GetAllAccounts();

                if (accounts == null)
                {
                    result.Success = false;
                    result.Message = "No accounts found.";
                }
                else
                {
                    result.Success = true;
                    result.Data = accounts;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error. Please try again later.";
                //log.logError(ex.Message);
            }

            return result;

        }


        public Response<Account> GetAccount(int accountNumber)
        {
            var repo = new AccountRepository();
            var result = new Response<Account>();

            try
            {
                var account = repo.LoadAccount(accountNumber);

                if (account == null)
                {
                    result.Success = false;
                    result.Message = "Account was not found.";
                }
                else
                {
                    result.Success = true;
                    result.Data = account;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error. Please try again later.";
                //log.logError(ex.Message);
            }

            return result;
        }

        public Response<Account> MakeAccount(Account newAccount)
        {
            var repo = new AccountRepository();
            var result = new Response<Account>();

            try
            {
                var account = repo.CreateAccount(newAccount);

                result.Success = true;
                result.Data = account;              
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error. Please try again later.";
                //log.logError(ex.Message);
            }

            return result;
        }

        public Response<int> RemoveAccount(int accountNumber)
        {
            var repo = new AccountRepository();
            var result = new Response<int>();

            try
            {
                bool deleteSuccess = repo.DeleteAccount(accountNumber);

                if (deleteSuccess) { 
                    result.Success = true;
                    result.Data = accountNumber;
                }
                else
                {
                    result.Success = false;
                    result.Message = "Account does not exist";
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error. Please try again later.";
            }

            return result;
        }


        public Response<DepositReciept> Deposit(decimal amount, Account account)
        {
            var response = new Response<DepositReciept>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must provide a positive value.";
                }
                else
                {
                    account.Balance += amount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);

                    response.Success = true;
                    response.Data = new DepositReciept();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.DepositAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Account is no longer valid.";
                //log.logError(ex.Message);
            }

            return response;
        }

        public Response<WithdrawReciept> Withdraw(decimal amount, Account account)
        {
            var response = new Response<WithdrawReciept>();

            try
            {
                if (amount <= 0)
                {
                    response.Success = false;
                    response.Message = "Must provide a positive value.";
                }
                else if (amount > account.Balance)
                {
                    response.Success = false;
                    response.Message = "Account balance is less than withdrawal ammount";
                }
                else
                {
                    account.Balance -= amount;
                    var repo = new AccountRepository();
                    repo.UpdateAccount(account);

                    response.Success = true;
                    response.Data = new WithdrawReciept();
                    response.Data.AccountNumber = account.AccountNumber;
                    response.Data.WithdrawAmount = amount;
                    response.Data.NewBalance = account.Balance;
                }
            }
            catch (Exception ex)
            {
                response.Success = false;
                response.Message = "Account is no longer valid.";
                //log.logError(ex.Message);
            }

            return response;
        }

        public Response<TransferReciept> Transfer(decimal amount, Account fromAccount, Account toAccount)
        {
            var response = new Response<TransferReciept>();


            if (fromAccount.AccountNumber == toAccount.AccountNumber)
            {
                response.Success = false;
                response.Message = "Can not transfer to the same account";
            }
            else
            {
                Response<WithdrawReciept> withdrawResponse = Withdraw(amount, fromAccount);

                if (withdrawResponse.Success)
                {
                    Response<DepositReciept> depositResponse = Deposit(amount, toAccount);

                    if (depositResponse.Success)
                    {
                        response.Success = true;
                        response.Data = new TransferReciept();
                        response.Data.AccountNumberFrom = fromAccount.AccountNumber;
                        response.Data.AccountNumberTo = toAccount.AccountNumber;
                        response.Data.Amount = amount;
                        response.Data.NewBalanceFromAccount = withdrawResponse.Data.NewBalance;
                        response.Data.NewBalanceToAccount = depositResponse.Data.NewBalance;
                    }
                    else
                    {
                        Response<DepositReciept> redepositResponse = Deposit(amount, fromAccount);
                        response.Success = false;
                        response.Message = "Transfer failed";
                    }
                }
                else
                {
                    response.Success = false;
                    response.Message = withdrawResponse.Message;
                }
            }

            return response;
        }

        public Response<Account> GetMaximumBalanceAccount()
        {
            var repo = new AccountRepository();
            var result = new Response<Account>();

            try
            {
                var accountsList = repo.GetAllAccounts();

                if (accountsList == null)
                {
                    result.Success = false;
                    result.Message = "There are no accounts";
                }
                else
                {
                    var maxAccount = accountsList.FirstOrDefault(a => a.Balance == accountsList.Max(b=>b.Balance));
                    result.Success = true;
                    result.Data = maxAccount;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error. Please try again later.";
                //log.logError(ex.Message);
            }

            return result;
        }

        public Response<Account> GetMinimumBalanceAccount()
        {
            var repo = new AccountRepository();
            var result = new Response<Account>();

            try
            {
                var accountsList = repo.GetAllAccounts();

                if (accountsList == null)
                {
                    result.Success = false;
                    result.Message = "There are no accounts";
                }
                else
                {
                    var maxAccount = accountsList.FirstOrDefault(a => a.Balance == accountsList.Min(b => b.Balance));
                    result.Success = true;
                    result.Data = maxAccount;
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = "There was an error. Please try again later.";
                //log.logError(ex.Message);
            }

            return result;
        }


    }
}
