﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGBank.Models
{
    public class TransferReciept
    {
        public int AccountNumberFrom { get; set; }
        public int AccountNumberTo { get; set; }
        public decimal Amount { get; set; }
        public decimal NewBalanceFromAccount { get; set; }
        public decimal NewBalanceToAccount { get; set; }
    }
}
