﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGBank.BLL;
using SGBank.Models;

namespace SGBank.Tests
{
    [TestFixture]
    public class AccountManagerTests
    {
        private Account _testAccount, _testAccount2;
        
        private AccountManager _manager;

        [SetUp]
        public void BeforeEachTest()
        {
            _testAccount = new Account()
            {
                FirstName = "Mike",
                LastName = "Popowicz",
                Balance = 2000
            };

            _testAccount2 = new Account()
            {
                FirstName = "Will",
                LastName = "Blair",
                Balance = 3000
            };

            _manager = new AccountManager();
        }

        [Test]
        public void FoundAccountReturnsSuccess()
        {
            var response = _manager.GetAccount(1);

            Assert.IsTrue(response.Success);
            Assert.AreEqual(1, response.Data.AccountNumber);
            Assert.AreEqual("Mary", response.Data.FirstName);
        }

        [Test]
        public void NotFoundAccountReturnsFail()
        {
            
            var response = _manager.GetAccount(9999);
            Assert.IsFalse(response.Success);
        }

        [Test]
        public void MakeAccountReturnsSuccess()
        {
            
            
            var response = _manager.MakeAccount(_testAccount);

            Assert.IsTrue(response.Success);
            Assert.AreEqual("Mike", response.Data.FirstName);
            Assert.AreEqual("Popowicz", response.Data.LastName);
            Assert.AreEqual(2000, response.Data.Balance);
        }

        [Test]
        public void RemoveAccountReturnsSuccess()
        {
            
            
            var makeAccountResponse = _manager.MakeAccount(_testAccount);

            Assert.IsTrue(makeAccountResponse.Success);
            Assert.AreEqual("Mike", makeAccountResponse.Data.FirstName);
            Assert.AreEqual("Popowicz", makeAccountResponse.Data.LastName);
            Assert.AreEqual(2000, makeAccountResponse.Data.Balance);

            var removeAccountResponse = _manager.RemoveAccount(makeAccountResponse.Data.AccountNumber);
            Assert.IsTrue(removeAccountResponse.Success);
            Assert.AreEqual(makeAccountResponse.Data.AccountNumber, removeAccountResponse.Data);
        }

        [Test]
        public void DepositReturnsSuccess()
        {
            
            
            var makeAccountResponse = _manager.MakeAccount(_testAccount);

            Assert.IsTrue(makeAccountResponse.Success);
            Assert.AreEqual("Mike", makeAccountResponse.Data.FirstName);
            Assert.AreEqual("Popowicz", makeAccountResponse.Data.LastName);
            Assert.AreEqual(2000, makeAccountResponse.Data.Balance);

            var depositResponse = _manager.Deposit(200, makeAccountResponse.Data);

            Assert.AreEqual(2200, depositResponse.Data.NewBalance);
        }


        [Test]
        public void ValidWithdrawReturnsSuccess()
        {
            

            var makeAccountResponse = _manager.MakeAccount(_testAccount);

            Assert.IsTrue(makeAccountResponse.Success);
            Assert.AreEqual("Mike", makeAccountResponse.Data.FirstName);
            Assert.AreEqual("Popowicz", makeAccountResponse.Data.LastName);
            Assert.AreEqual(2000, makeAccountResponse.Data.Balance);

            var withdrawResponse = _manager.Withdraw(200, makeAccountResponse.Data);

            Assert.AreEqual(1800, withdrawResponse.Data.NewBalance);
        }

        [Test]
        public void WithdrawIsMoreThanBalance()
        {
            

            var makeAccountResponse = _manager.MakeAccount(_testAccount);

            Assert.IsTrue(makeAccountResponse.Success);
            Assert.AreEqual("Mike", makeAccountResponse.Data.FirstName);
            Assert.AreEqual("Popowicz", makeAccountResponse.Data.LastName);
            Assert.AreEqual(2000, makeAccountResponse.Data.Balance);

            var withdrawResponse = _manager.Withdraw(20000, makeAccountResponse.Data);

            Assert.IsFalse(withdrawResponse.Success);
        }

        [Test]
        public void TransferIsSuccess()
        {
            var makeAccountResponse = _manager.MakeAccount(_testAccount);
            var makeToAccountResponse = _manager.MakeAccount(_testAccount2);

            var TransferAccountResponse = _manager.Transfer(1000, _testAccount, _testAccount2);
           
            Assert.AreEqual(1000, TransferAccountResponse.Data.NewBalanceFromAccount);
            Assert.AreEqual(4000, TransferAccountResponse.Data.NewBalanceToAccount);
        }

        [Test]
        public void TransferToSameAccount()
        {
            var makeAccountResponse = _manager.MakeAccount(_testAccount);

            var TransferAccountResponse = _manager.Transfer(1000, _testAccount, _testAccount);

            Assert.IsFalse(TransferAccountResponse.Success);
            Assert.AreEqual("Can not transfer to the same account", TransferAccountResponse.Message);
        }


    }
}
