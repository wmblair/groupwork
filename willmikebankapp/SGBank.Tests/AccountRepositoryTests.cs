﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SGBank.Data;
using SGBank.Models;

namespace SGBank.Tests
{
    [TestFixture]
    public class AccountRepositoryTests
    {
        [Test]
        public void CanLoadAllAccounts()
        {
            var repo = new AccountRepository();
            var accounts = repo.GetAllAccounts();

            Assert.AreEqual(4,accounts.Count);
        }

        [TestCase(1, "Mary")]
        [TestCase(2, "Bob")]
        public void CanLoadSpecificAccount(int accountNumber, string expected)
        {
            var repo = new AccountRepository();
            var account = repo.LoadAccount(accountNumber);

            Assert.AreEqual(expected, account.FirstName);
        }

        [Test]
        public void UpdateAccountSucceeds()
        {
            var repo = new AccountRepository();
            var accountToUpdate = repo.LoadAccount(1);
            accountToUpdate.Balance = 500.00m;
            repo.UpdateAccount(accountToUpdate);

            var result = repo.LoadAccount(1);
            Assert.AreEqual(500.00m, result.Balance);
        }

        [Test]
        public void CreateAccountSucceeds()
        {
            var repo = new AccountRepository();
            Account newAccount = new Account()
            {
                FirstName = "Mike", 
                LastName = "Popowicz",
                Balance = 2000
            };
            
            var createdAccount = repo.CreateAccount(newAccount);
            
            Assert.AreEqual(newAccount.FirstName, createdAccount.FirstName);
            Assert.AreEqual(newAccount.LastName, createdAccount.LastName);
            Assert.AreEqual(newAccount.Balance, createdAccount.Balance);
        }

        [Test]
        public void CreateAccountSelectsNextID()
        {
            var repo = new AccountRepository();
            Account newAccount = new Account()
            {
                FirstName = "Mike",
                LastName = "Popowicz",
                Balance = 2000
            };

            int nextID = repo.GetAllAccounts().Select(a => a.AccountNumber).Max() + 1;

            var createdAccount = repo.CreateAccount(newAccount);

            Assert.AreEqual(nextID, createdAccount.AccountNumber);
            
        }

        [Test]
        public void DeleteAccountSucceeds()
        {
            var repo = new AccountRepository();
            Account newAccount = new Account()
            {
                FirstName = "Mike",
                LastName = "Popowicz",
                Balance = 2000
            };

            var createdAccount = repo.CreateAccount(newAccount);

            Assert.AreEqual(newAccount.FirstName, createdAccount.FirstName);
            Assert.AreEqual(newAccount.LastName, createdAccount.LastName);
            Assert.AreEqual(newAccount.Balance, createdAccount.Balance);

            repo.DeleteAccount(createdAccount.AccountNumber);

            Assert.IsNull(repo.LoadAccount(createdAccount.AccountNumber));

        }

        [TestCase(0)]
        [TestCase(133)]
        public void DeleteAccountDoesntExist(int accountNumber)
        {
            var repo = new AccountRepository();
            
            var result = repo.DeleteAccount(accountNumber);

            Assert.IsFalse(result);

        }

    }

}

