﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.Models;

namespace SGBank.UI.Utilities
{
    public static class AccountScreens
    {
        public static void PrintAccountDetails(Account account)
        {
            Console.WriteLine("Account Information");
            Console.WriteLine("===============================");
            Console.WriteLine($"Account Number {account.AccountNumber}");
            Console.WriteLine($"Name {account.FirstName} {account.LastName}");
            Console.WriteLine($"Account Balance {account.Balance:c}");
        }

        public static void WorkflowErrorScreen(string message)
        {
            Console.Clear();
            Console.WriteLine("An error occured. {0}", message);
            UserPrompts.PressKeyForContinue();
        }

        public static void DepositDetails(DepositReciept reciept)
        {
            Console.Clear();
            Console.WriteLine("Deposited {0:c} to account {1}.", 
                reciept.DepositAmount,
                reciept.AccountNumber);
            Console.WriteLine("New Balance is {0}", reciept.NewBalance);
            UserPrompts.PressKeyForContinue();
        }

        public static void WithdrawDetails(WithdrawReciept reciept)
        {
            Console.Clear();
            Console.WriteLine("Withdrew {0:c} from account {1}.",
                reciept.WithdrawAmount,
                reciept.AccountNumber);
            Console.WriteLine("New Balance is {0}", reciept.NewBalance);
            UserPrompts.PressKeyForContinue();
        }

        public static void TransferDetails(TransferReciept reciept)
        {
            Console.Clear();
            Console.WriteLine("Transfer was a success");
            Console.WriteLine("\nWithdrew {0:c} from account {1}.",
                reciept.Amount,
                reciept.AccountNumberFrom);
            Console.WriteLine("Deposit {0:c} to account {1}.",
                reciept.Amount,
                reciept.AccountNumberTo);
            Console.WriteLine("\nNew Balance in account {0} is: {1}", 
                reciept.AccountNumberFrom, reciept.NewBalanceFromAccount);
            Console.WriteLine("New Balance in account {0} is: {1}",
                reciept.AccountNumberTo, reciept.NewBalanceToAccount);
            UserPrompts.PressKeyForContinue();
        }

        public static void DeleteDetails(int accountNumber)
        {
            Console.Clear();
            Console.WriteLine("Deleted account {0}.", accountNumber);
            UserPrompts.PressKeyForContinue();
        }

        public static void ListAllAccountsScreen(List<Account> accountsList)
        {
            Console.Clear();
            Console.WriteLine("List of all accounts");
            Console.WriteLine("=========================");
            Console.WriteLine("{0, -4} {1, -15} {2, -15} {3, 10}", "No.", "First Name", "Last Name", "Balance");

            foreach (var account in accountsList)
            {
                Console.WriteLine("{0, -4} {1, -15} {2, -15} {3, 10:c}", 
                    account.AccountNumber, account.FirstName, account.LastName, account.Balance);
            }
            UserPrompts.PressKeyForContinue();
        }

        public static void MaxBalanceAccountScreen(Account account)
        {
            Console.Clear();
            Console.WriteLine("Account with Maximum Value");
            Console.WriteLine("=========================");
            Console.WriteLine("{0, -4} {1, -15} {2, -15} {3, 10}", "No.", "First Name", "Last Name", "Balance");
            Console.WriteLine("{0, -4} {1, -15} {2, -15} {3, 10:c}",
                    account.AccountNumber, account.FirstName, account.LastName, account.Balance);

            UserPrompts.PressKeyForContinue();
        }

        public static void MinBalanceAccountScreen(Account account)
        {
            Console.Clear();
            Console.WriteLine("Account with Minimum Value");
            Console.WriteLine("=========================");
            Console.WriteLine("{0, -4} {1, -15} {2, -15} {3, 10}", "No.", "First Name", "Last Name", "Balance");
            Console.WriteLine("{0, -4} {1, -15} {2, -15} {3, 10:c}",
                    account.AccountNumber, account.FirstName, account.LastName, account.Balance);

            UserPrompts.PressKeyForContinue();
        }

    }
}
