﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    public class CreateAccountWorkflow
    {
        private Account _newAccount;

        public void Execute()
        {
            string firstName = UserPrompts.GetStringFromUser("Enter your first name: ");
            string lastName = UserPrompts.GetStringFromUser("Enter your last name: ");

            decimal amount = UserPrompts.GetDecimalFromUser("Enter an amount you would like to put in your new account: ");

            _newAccount = new Account()
            {
                FirstName = firstName,
                LastName = lastName,
                Balance = amount
            };

            AccountManager am = new AccountManager();

            var response = am.MakeAccount(_newAccount);
            
            var LookupWorkflow = new LookupWorkflow();

            LookupWorkflow.Execute(response.Data.AccountNumber);


        }
    }
}
