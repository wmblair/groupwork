﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    public class DeleteAccountWorkflow
    {
        public void Execute()
        {
            int account = UserPrompts.GetIntFromUser("Enter an account to remove: ");

            AccountManager am = new AccountManager();

            var response = am.RemoveAccount(account);

            if (response.Success)
            {
                AccountScreens.DeleteDetails(response.Data);
            }
            else
            {
                AccountScreens.WorkflowErrorScreen(response.Message);
            }

        }
    }
}
