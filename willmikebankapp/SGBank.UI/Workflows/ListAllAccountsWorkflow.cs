﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    public class ListAllAccountsWorkflow
    {
        public void Execute()
        {
            AccountManager manager = new AccountManager();
            var response = manager.GetAllAccounts();

            if (response.Success)
            {
                AccountScreens.ListAllAccountsScreen(response.Data);
            }
            else
            {
                AccountScreens.WorkflowErrorScreen(response.Message);
            }
        }

         
    }
}
