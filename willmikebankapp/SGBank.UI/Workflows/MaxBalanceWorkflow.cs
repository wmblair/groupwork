﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    public class MaxBalanceWorkflow
    {
        public void Execute()
        {
            AccountManager manager = new AccountManager();
            var response = manager.GetMaximumBalanceAccount();

            if (response.Success)
            {
                AccountScreens.MaxBalanceAccountScreen(response.Data);
            }
            else
            {
                AccountScreens.WorkflowErrorScreen(response.Message);
            }
        }
    }
}
