﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    public class ReportsWorkflow
    {
        public void Execute()
        {
            DisplayReportsMenu();
        }

        private void DisplayReportsMenu()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Reports Menu");
                Console.WriteLine("==================================");
                Console.WriteLine("\n1. List All Accounts");
                Console.WriteLine("2. Find Max Balance");
                Console.WriteLine("3. Find Min Balance");
                Console.WriteLine("\n(Q) to return to main menu");

                string input = UserPrompts.GetStringFromUser("\nEnter Choice: ");

                if (input.Length > 0 && input.Substring(0, 1).ToUpper() == "Q")
                    break;

                ProcessChoice(input);

            } while (true);
        }

        private void ProcessChoice(string choice)
        {
            switch (choice)
            {
                case "1":
                    ListAllAccountsWorkflow allAccounts = new ListAllAccountsWorkflow();
                    allAccounts.Execute();
                    break;
                case "2":
                    MaxBalanceWorkflow maxBalanceWorkflow = new MaxBalanceWorkflow();
                    maxBalanceWorkflow.Execute();
                    break;
                case "3":
                    MinBalanceWorkflow minBalanceWorkflow = new MinBalanceWorkflow();
                    minBalanceWorkflow.Execute();
                    break;
            }
        }
    }
}
