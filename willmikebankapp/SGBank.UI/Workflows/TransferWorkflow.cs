﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGBank.BLL;
using SGBank.Models;
using SGBank.UI.Utilities;

namespace SGBank.UI.Workflows
{
    class TransferWorkflow
    {
        public void Execute(Account account)
        {
            decimal amount = UserPrompts.GetDecimalFromUser("Please provide a transfer amount:");

            int toAccountNumber = UserPrompts.GetIntFromUser("Please enter account number to transfer to: ");
            
            var manager = new AccountManager();
            var toAccount = manager.GetAccount(toAccountNumber);

            if (toAccount.Success)
            {
                var response = manager.Transfer(amount, account, toAccount.Data);

                if (response.Success)
                {
                    AccountScreens.TransferDetails(response.Data);
                }
                else
                {
                    AccountScreens.WorkflowErrorScreen(response.Message);
                }
            }
            else
            {
                AccountScreens.WorkflowErrorScreen(toAccount.Message);
            }



        }
    }
}
