***Main Menu Options***

Feature: Create Account
Narrative: As a bank teller I want to be able to create an account

Scenario: Create account with name and opening balance
	Given: A name and opening balance
	When: I enter a name and opening balance
	Then: Create a unique account number
	And: Assign name
	And: Opening balance


Feature: Delete Account
Narrative: As a bank teller I want to be able to delete an account

Scenario: Delete account with valid account number
	Given: A valid account number
	When: I enter the account number in the lookup screen
	Then: I create a withdrawal of the total balnce
	And: The system deletes the account


Feature: Lookup Account
Narrative: As a bank teller I want to look up accounts

Scenario: Lookup account with valid account number
	Given: I have a valid account number
	When: I enter the account number in the lookup screen
	Then: The system will display account details including balance
	And: I will be presented with a menu of option for this account


		**In Account Information Screen**

		Feature: Deposit
		Narrative: As a bank teller I can enter a deposit for a customer

		Scenario: Add deposit to specified account

			Given: A valid account number
			When: I enter a deposit
			Then: The system updates the specified account
			And: The system returns a deposit receipt

		Feature: Withdraw
		Narrative: As a bank teller I can enter a withdrawal for a customer

		Scenario: Withdraw amount from specified account

			Given: A valid account number
			When: I enter a withdrawal amount
			Then: The system updates the specified account
			And: The system returns a withdrawal receipt
			
		Feature: Transfer
		Narrative: As a bank teller I can enter a transfer for a customer

		Scenario: Transfer funds from selecet account to another account

			Given: A valid account number			
			When: I select another valid account number
			And: I enter a tranfer amount
			Then: The system updates the specified accounts
			And: The system returns a transfer receipt